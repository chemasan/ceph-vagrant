#!/usr/bin/env bash

# Ref: https://docs.ceph.com/en/latest/install/manual-deployment/
# Ref: https://docs.ceph.com/en/latest/mgr/administrator/#mgr-administrator-guide
# Ref: https://docs.ceph.com/en/latest/cephfs/createfs/
# Ref: https://docs.ceph.com/en/latest/rados/operations/add-or-rm-mons/
# Ref: https://docs.ceph.com/en/latest/rados/operations/add-or-rm-osds/

set -e

apt-get update
apt-get install -y ceph radosgw ceph-mgr-dashboard s3cmd jq
apt-get purge -y unattended-upgrades

# Configure the MON daemon
declare fsid=$(uuidgen)
declare hostname=$(hostname -s)
declare ip=$(ip -4 -j -p address show eth1 scope global | jq -r .[0].addr_info[0].local)
declare net=$(ip -j -p route show dev eth1 scope link | jq -r .[0].dst)

cat > /etc/ceph/ceph.conf << EOF
[global]
fsid = ${fsid}
mon_initial_members = ${hostname}
mon_host = ${ip}
auth_cluster required = cephx
auth_service required = cephx
auth_client  required = cephx
osd_pool_default_size     = 1
osd_pool_default_min_size = 1
osd_crush_chooseleaf_type = 1

[mon.${hostname}]
host = ${hostname}
mon addr = ${ip}:6789
EOF

ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' --cap mgr 'allow r'
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
chown ceph:ceph /tmp/ceph.mon.keyring
monmaptool --create --add ${hostname} ${ip} --fsid ${fsid} /tmp/monmap
sudo -u ceph mkdir /var/lib/ceph/mon/ceph-${hostname}
sudo -u ceph ceph-mon --mkfs -i ${hostname} --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring
systemctl enable ceph-mon@${hostname} --now

# Configure OSD daemons
ceph-volume lvm create --data /dev/vdb
ceph-volume lvm create --data /dev/vdc

# Configure the MGR daemon
sudo -u ceph mkdir /var/lib/ceph/mgr/ceph-${hostname}
ceph auth get-or-create mgr.${hostname} mon 'allow profile mgr' osd 'allow *' mds 'allow *' -o /var/lib/ceph/mgr/ceph-${hostname}/keyring
chown -R ceph:ceph /var/lib/ceph/mgr/ceph-${hostname}
systemctl enable ceph-mgr@${hostname} --now

# Configure the MDS daemon
cat >> /etc/ceph/ceph.conf << EOF

[mds.${hostname}]
host = ${hostname}
EOF

mkdir -p /var/lib/ceph/mds/ceph-${hostname}
ceph auth get-or-create mds.${hostname} osd "allow rwx" mds "allow *" mon "allow profile mds" -o /var/lib/ceph/mds/ceph-${hostname}/keyring
chown -R ceph:ceph /var/lib/ceph/mds/ceph-${hostname}
systemctl enable ceph-mds@${hostname} --now

# Create a CephFS filesystem
ceph osd pool create cephfs.data
ceph osd pool create cephfs.metadata
ceph fs new cephfs cephfs.metadata cephfs.data
sleep 10

# Create a client and mount the CephFS filesystem
mkdir -p /mnt/cephfs
declare user1key=$(ceph auth get-or-create-key client.user1 mon 'allow r' mds 'allow rw' osd 'allow rw pool=cephfs.data')
echo "${ip}:6789:/              /mnt/cephfs/    ceph     name=user1,secret=${user1key},noatime    0 0" >> /etc/fstab
systemctl daemon-reload
mount -a

# Fix warnings
ceph config set mon auth_allow_insecure_global_id_reclaim false
ceph config set mon auth_client_required cephx
ceph mgr module disable restful
ceph mon enable-msgr2
ceph health mute POOL_NO_REDUNDANCY

# Enable dashboard
ceph mgr module enable dashboard
ceph dashboard create-self-signed-cert
ceph dashboard ac-user-create admin administrator -i - <<< Patata01
sleep 10
ceph mgr services  #display the URL

